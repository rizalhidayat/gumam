//
//  Song.swift
//  Gumam
//
//  Created by Rizal Hidayat on 10/05/24.
//

import Foundation

struct Song: Decodable, Hashable {
    let artistName: String?
    let trackName: String?
    let collectionName: String?
    let previewUrl: String?
    let artworkUrl100: String?
}
