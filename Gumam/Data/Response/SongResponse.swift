//
//  SongResponse.swift
//  Gumam
//
//  Created by Rizal Hidayat on 10/05/24.
//

import Foundation

struct SongResponse: Decodable {
    let songs: [Song]?
    
    enum CodingKeys: String, CodingKey {
        case songs = "results"
    }
}
