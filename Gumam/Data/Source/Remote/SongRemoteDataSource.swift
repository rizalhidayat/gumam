//
//  SongRemoteDataSource.swift
//  Gumam
//
//  Created by Rizal Hidayat on 10/05/24.
//

import Foundation
import Combine

protocol SongRemoteDataSourceProtocol {
    func getSongs(keyword: String) -> AnyPublisher<SongResponse, APIError>
}

class SongRemoteDataSource: SongRemoteDataSourceProtocol {
    static let shared = SongRemoteDataSource()
    func getSongs(keyword: String) -> AnyPublisher<SongResponse, APIError> {
        let urlString = "https://itunes.apple.com/search"
        let url = URL(string: urlString)!
        var components = URLComponents(url: url, resolvingAgainstBaseURL: true)!
        let queryItems: [URLQueryItem] = [
          URLQueryItem(name: "term", value: keyword),
          URLQueryItem(name: "limit", value: "15"),
          URLQueryItem(name: "media", value: "music")
        ]
        components.queryItems = components.queryItems.map { $0 + queryItems } ?? queryItems
        let request = URLRequest(url: components.url!)
        return APIService.shared.request(request)
    }
}
