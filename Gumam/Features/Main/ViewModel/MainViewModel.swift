//
//  MainViewModel.swift
//  Gumam
//
//  Created by Rizal Hidayat on 10/05/24.
//

import Foundation
import AVFoundation
import Combine

class MainViewModel {
    private let repository: SongRepositoryProtocol = SongRepository.shared
    private let player = AVPlayer()
    private var cancellables = Set<AnyCancellable>()
    @Published var songs: [Song] = []
    @Published var errorMessage: String?
    
    @Published var selectedSongIndex: IndexPath?
    @Published var isSongSelected: Bool = false
    @Published var isSongPlaying: Bool = false
    @Published var duration: Double = .zero
    @Published var currentTime: Double = .zero
    
    init() {
        player.publisher(for: \.currentItem)
            .sink { [weak self] item in
                if let item {
                    self?.isSongSelected = true
                   
                    self?.observeDuration(for: item)
                } else {
                    self?.isSongSelected = false
                }
            }.store(in: &cancellables)
        
        player.addPeriodicTimeObserver(forInterval: CMTime(seconds: 0.1, preferredTimescale: CMTimeScale(NSEC_PER_SEC)), queue: DispatchQueue.main) { [weak self] time in
            self?.currentTime = CMTimeGetSeconds(time)
        }

        
        player.publisher(for: \.timeControlStatus)
            .sink { [weak self] status in
                self?.isSongPlaying = status == .playing
            }
            .store(in: &cancellables)
    }
    
    private func observeDuration(for item: AVPlayerItem) {
        item.publisher(for: \.duration, options: .new)
            .sink { [weak self] duration in
                self?.duration = CMTimeGetSeconds(duration)
            }
            .store(in: &cancellables)
    }
    
    func fetchSong(keyword: String) {
        repository.fetchSongs(keyword: keyword)
            .sink { completion in
                switch completion {
                case .finished:
                    print("fetch finished")
                case .failure(let error):
                    self.errorMessage = error.localizedDescription
                }
            } receiveValue: { songs in
                self.songs = songs
            }.store(in: &cancellables)
    }
    
    func selectSong(index: IndexPath) {
        guard let urlString = songs[index.row].previewUrl, let url = URL(string: urlString) else {
            return
        }
        selectedSongIndex = index
        let item = AVPlayerItem(url: url)
        player.replaceCurrentItem(with: item)
        player.play()
    }
    
    func playNextSong() {
        guard let selectedSongIndex = selectedSongIndex else {
            return
        }
        
        let nextIndex = (selectedSongIndex.row + 1) % songs.count
        guard let urlString = songs[nextIndex].previewUrl, let url = URL(string: urlString) else {
            return
        }
        
        let item = AVPlayerItem(url: url)
        player.replaceCurrentItem(with: item)
        player.play()
        
        self.selectedSongIndex = IndexPath(row: nextIndex, section: selectedSongIndex.section)
    }

    func playPrevSong() {
        guard let selectedSongIndex = selectedSongIndex else {
            return
        }
        
        let count = songs.count
        let prevIndex = (selectedSongIndex.row - 1 + count) % count
        guard let urlString = songs[prevIndex].previewUrl, let url = URL(string: urlString) else {
            return
        }
        
        let item = AVPlayerItem(url: url)
        player.replaceCurrentItem(with: item)
        player.play()
        
        self.selectedSongIndex = IndexPath(row: prevIndex, section: selectedSongIndex.section)
    }

    
    func clearSelectedSong() {
        player.replaceCurrentItem(with: nil)
    }
    
    func seekSong(value: Double) {
        let time = CMTime(seconds: value, preferredTimescale: CMTimeScale(NSEC_PER_SEC))
        player.seek(to: time, toleranceBefore: .zero, toleranceAfter: .zero)
    }

    
    func playPauseSong() {
        if isSongPlaying {
            player.pause()
        } else {
            player.play()
        }
    }
}
