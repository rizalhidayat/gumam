//
//  SongTableViewCell.swift
//  Gumam
//
//  Created by Rizal Hidayat on 10/05/24.
//

import UIKit
import Kingfisher

class SongTableViewCell: UITableViewCell {

    @IBOutlet weak var albumImageView: UIImageView!
    
    @IBOutlet weak var trackNameLabel: UILabel!
    @IBOutlet weak var artistNameLabel: UILabel!
    @IBOutlet weak var albumNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(with song: Song) {
        trackNameLabel.text = song.trackName
        artistNameLabel.text = song.artistName
        albumNameLabel.text = song.collectionName
        if let urlString = song.artworkUrl100, let url = URL(string: urlString) {
            albumImageView.kf.setImage(with: url)
        }
    }
    
}
