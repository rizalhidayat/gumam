//
//  MainViewController.swift
//  Gumam
//
//  Created by Rizal Hidayat on 09/05/24.
//

import UIKit
import Combine

class MainViewController: UIViewController {
    @IBOutlet weak var songTableView: UITableView!
    @IBOutlet weak var backwardButton: UIButton!
    @IBOutlet weak var playPauseButton: UIButton!
    @IBOutlet weak var fowardButton: UIButton!
    @IBOutlet weak var playbackView: UIView!
    @IBOutlet weak var playbackSlider: UISlider!
    private var isSliderDragged = false
    
    private lazy var searchController: UISearchController = {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.delegate = self
        return searchController
    }()
    
    private lazy var dataSource = makeDataSource()
    private var keyword = PassthroughSubject<String, Never>()
    private var cancellables = Set<AnyCancellable>()
    var viewModel: MainViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initNavigationBar()
        initTableView()
        initPlaybackButtons()
        initSubscribers()
    }
    
    private func initNavigationBar() {
        definesPresentationContext = true
        title = "Gumam App"
        navigationItem.searchController = searchController
    }
    
    private func initTableView() {
        songTableView.register(cellClass: SongTableViewCell.self)
        songTableView.delegate = self
        songTableView.dataSource = makeDataSource()
    }
    
    private func initPlaybackButtons() {
        playPauseButton.addTarget(self, action: #selector(playOrPause), for: .touchUpInside)
        playbackSlider.addTarget(self, action: #selector(sliderDidBeginDragging), for: .touchDown)
        playbackSlider.addTarget(self, action: #selector(sliderDidEndDragging(_:)), for: [.touchUpInside, .touchUpOutside])
        fowardButton.addTarget(self, action: #selector(playNextSong), for: .touchUpInside)
        backwardButton.addTarget(self, action: #selector(playPrevSong), for: .touchUpInside)
    }
    
    private func initSubscribers() {
        keyword.receive(on: DispatchQueue.main)
            .debounce(for: .seconds(0.5), scheduler: DispatchQueue.main)
            .sink { [weak self] searchText in
                self?.viewModel.fetchSong(keyword: searchText)
            }
            .store(in: &cancellables)
        viewModel.$songs.receive(on: DispatchQueue.main)
            .dropFirst()
            .sink { [weak self] songs in
                self?.update(with: songs)
            }
            .store(in: &cancellables)
        viewModel.$isSongSelected.receive(on: DispatchQueue.main)
            .map{ !$0 }
            .assign(to: \.isHidden, on: playbackView)
            .store(in: &cancellables)
        playbackView.publisher(for: \.isHidden)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] status in
                if status {
                    self?.songTableView.contentInset.bottom = 0
                } else {
                    self?.songTableView.contentInset.bottom = self?.playbackView.frame.height ?? 0
                }
            }.store(in: &cancellables)
        viewModel.$isSongPlaying.receive(on: DispatchQueue.main)
            .sink { [weak self] status in
                    let image = status ? UIImage(systemName: "pause.fill") : UIImage(systemName: "play.fill")
                    self?.playPauseButton.setImage(image, for: .normal)
            }.store(in: &cancellables)
        viewModel.$duration.receive(on: DispatchQueue.main)
            .filter { $0 > 0.0 }
            .sink { [weak self] duration in
                self?.playbackSlider.maximumValue = Float(duration)
            }.store(in: &cancellables)
        viewModel.$currentTime.receive(on: DispatchQueue.main)
            .sink { [weak self] currentTime in
                if let isDragged = self?.isSliderDragged, isDragged {
                    return
                }
                self?.playbackSlider.setValue(Float(currentTime), animated: true)
            }.store(in: &cancellables)
        viewModel.$selectedSongIndex.receive(on: DispatchQueue.main)
            .sink { [weak self] IndexPath in
                self?.songTableView.selectRow(at: IndexPath, animated: true, scrollPosition: .none)
            }.store(in: &cancellables)
    }
    
    //MARK: - Listener
    @objc
    private func playOrPause() {
        viewModel.playPauseSong()
    }
    
    @objc 
    private func sliderDidBeginDragging() {
        isSliderDragged = true
    }

    @objc 
    private func sliderDidEndDragging(_ slider: UISlider) {
        let value = slider.value
        viewModel.seekSong(value: Double(value))
        isSliderDragged = false
    }
    
    @objc
    private func playNextSong() {
        viewModel.playNextSong()
    }
    
    @objc
    private func playPrevSong() {
        viewModel.playPrevSong()
    }
    
}

extension MainViewController: UISearchBarDelegate {
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        viewModel.clearSelectedSong()
        keyword.send(searchText)
    }
    
    public func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        viewModel.clearSelectedSong()
        keyword.send("")
    }
}

//MARK: TableView Datasource
extension MainViewController {
    private enum Section: CaseIterable {
        case songs
    }
    
    private func makeDataSource() -> UITableViewDiffableDataSource<Section, Song> {
        return UITableViewDiffableDataSource(tableView: songTableView) { tableView, indexPath, song in
            let cell = tableView.dequeue(cellClass: SongTableViewCell.self, forIndexPath: indexPath)
            cell.configure(with: song)
            return cell
        }
    }
    
    private func update(with songs: [Song], animate: Bool = true) {
        DispatchQueue.main.async {
            var snapshot = NSDiffableDataSourceSnapshot<Section, Song>()
            snapshot.appendSections(Section.allCases)
            snapshot.appendItems(songs, toSection: .songs)
            self.dataSource.apply(snapshot, animatingDifferences: animate)
        }
    }
}

extension MainViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.selectSong(index: indexPath)
    }
}
