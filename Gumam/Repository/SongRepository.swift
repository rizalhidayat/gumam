//
//  SongRepository.swift
//  Gumam
//
//  Created by Rizal Hidayat on 10/05/24.
//

import Foundation
import Combine

protocol SongRepositoryProtocol {
    func fetchSongs(keyword: String) -> AnyPublisher<[Song], Error>
}

class SongRepository: SongRepositoryProtocol {
    static let shared = SongRepository()
    private let remoteDataSource: SongRemoteDataSourceProtocol = SongRemoteDataSource.shared
    
    func fetchSongs(keyword: String) -> AnyPublisher<[Song], Error> {
        return remoteDataSource.getSongs(keyword: keyword)
            .compactMap { $0.songs }
            .mapError { $0 as Error }
            .eraseToAnyPublisher()
    }
    
    
}
