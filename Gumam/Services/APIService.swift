//
//  APIService.swift
//  Gumam
//
//  Created by Rizal Hidayat on 10/05/24.
//

import Foundation
import Combine

public enum APIError: Error {
    case invalidURL
    case networkError(Error)
    case invalidResponse
}

class APIService {
    static let shared = APIService()
   
    func request<T: Decodable>(_ request: URLRequest) -> AnyPublisher<T, APIError> {
        return URLSession.shared.dataTaskPublisher(for: request)
            .mapError { error in
                APIError.networkError(error)
            }
            .flatMap { data, response -> AnyPublisher<T, APIError> in
                guard let httpResponse = response as? HTTPURLResponse, 200..<300 ~= httpResponse.statusCode else {
                    return Fail(error: APIError.invalidResponse).eraseToAnyPublisher()
                }

                return Just(data)
                    .decode(type: T.self, decoder: JSONDecoder())
                    .mapError { _ in APIError.invalidResponse }
                    .eraseToAnyPublisher()
            }
            .eraseToAnyPublisher()
    }
}

